from docx import Document
import docx
import pathlib
import toml
import sys
from datetime import date
from docx.shared import Inches

CONFIG_FILE = 'autodocx.toml'


def check_config_exists(config_dir: str = "") -> tuple[str, bool]:
    if not config_dir:
        config_dir = str(pathlib.Path.cwd())
    config_file_path = f"{config_dir}/{CONFIG_FILE}"
    if not pathlib.Path(config_file_path).is_file():
        return (config_file_path, False)
    return (config_file_path, True)


def create_config(config_dir: str = "") -> None:
    config_path, exists = check_config_exists(config_dir)
    if not exists:
        config = {
            "file": "",
            "front": {
                "title": "",
                "description": "",
                "author": "",
                "version": "1",
                "picture": "",
                "date": date.today(),
            }
        }
        with open(config_path, "w") as file:
            toml.dump(config, file)
            print(f"Configuration file created at: {config_path}")
            sys.exit(0)
    print(f"File exist: {config_path}")
    # Dialog to edit?
    sys.exit(0)


def front_metadata(author: str, version: str, date: str) -> str:
    keys_values = locals()
    return "\n".join(
        [f"{key}: {val}" for key, val in keys_values.items()]
    )


def docx_front(document: docx.document.Document,
               title: str,
               description: str,
               author: str,
               version: str,
               picture: str,
               date: str) -> docx.document.Document:
    document.add_heading(title, 0)
    document.add_paragraph(description)
    document.add_picture(picture, width=Inches(6))
    footer = document.sections[0].footer
    footer.paragraphs[0].add_run(
        front_metadata(author, version, date)
    )
    document.add_page_break()
    return document


def create_front(config_dir: str = ""):
    config_path, exists = check_config_exists(config_dir)
    if not exists:
        print(f"No configuration file exists at: {config_dir}")
        # Dialog to create?
        sys.exit(0)
    with open(config_path, "r") as file:
        config = toml.load(file)
        front = config["front"]
    docx_path = f"{config_dir}/{config['file']}"
    document = Document()
    document = docx_front(document=document, **front)
    document.save(config["file"])
    print(f"Docx created at: {docx_path}")
    sys.exit(0)


if __name__ == '__main__':
    # create_config()
    create_front()
