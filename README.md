# Automate your docx documentation

I don't like docx but I like python. So why not write something as an example for my friends how to do [busy work](https://en.wikipedia.org/wiki/Busy_work) automagically.

## Getting started

``` shell
git clone git@gitlab.com:theintegrative/automate-your-docx-documentation.git
```

